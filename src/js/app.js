var gamedevatApp = angular.module('gamedevatApp', ['ngRoute']);

gamedevatApp.config(['$routeProvider',
    function ($routeProvider) {
		$routeProvider.when('/companies', {
			templateUrl: 'partials/search.html',
			controller: 'SearchResultsCtrl'
		}).when('/companies/tag/:tag', {
			templateUrl: 'partials/search.html',
			controller: 'SearchResultsCtrl'
		}).when('/companies/:search', {
			templateUrl: 'partials/search.html',
			controller: 'SearchResultsCtrl'
		}).when('/company/new', {
			templateUrl: 'partials/newcompany.html',
			controller: 'NewCompanyCtrl'
		}).when('/company/:companyId', {
			templateUrl: 'partials/company.html',
			controller: 'CompanyCtrl'
		}).when('/edit/:companyId', {
			templateUrl: 'partials/newcompany.html',
			controller: 'NewCompanyCtrl'
		}).when('/impressum', {
			templateUrl: 'partials/impressum.html',
			controller: 'ImpressumCtrl'
		}).when('/home', {
			templateUrl: 'partials/home.html',
			controller: 'HomeCtrl'
		}).when('/events', {
			templateUrl: 'partials/events.html',
			controller: 'EventsCtrl'
		}).when('/data', {
			templateUrl: 'partials/dataoutput.html',
			controller: 'DataCtrl'
		}).when('/enableadmin', {
			templateUrl: 'partials/enableadmin.html',
			controller: function ($scope, $route, $location, $rootScope, $timeout) {
				$rootScope.isAdminmode = true;
				trackCurrentPage($location);
				$timeout(function () {
					$rootScope.isAdminmode = true;
					$location.path('/');
					$scope.$digest();
					$rootScope.$apply();
				}, 3000, false);
			}
		}).otherwise({
			redirectTo: '/home'
		});
    }]);

//setup for SEO
/*
gamedevatApp.config(['$location',
	function ($location) {
		$location.hashPrefix('!');
}]);
*/

gamedevatApp.directive("listitemcompany", function () {
	return {
		restrict: "E",
		templateUrl: "partials/listitemcompany.html"
	};
});

gamedevatApp.directive("mainmenu", function () {
	return {
		restrict: "E",
		templateUrl: "partials/menu.html"
	};
});

gamedevatApp.directive("banners", function () {
	return {
		restrict: "E",
		templateUrl: "partials/banner.html"
	};
});

gamedevatApp.run(function ($rootScope) {
	$rootScope.isAdminmode = false;
	//initially fill DB with data
	initializeDatabase();
});