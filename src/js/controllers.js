gamedevatApp.controller('MenuCtrl', function ($scope, $location) {
	$scope.getClass = function (path) {
		if ($location.path().substr(0, path.length) == path) {
			return "active"
		} else {
			return ""
		}
	}
	trackCurrentPage($location);
});

gamedevatApp.controller('DataCtrl', function ($scope, $filter, $location) {

	$scope.data = $filter('orderBy')(database, 'title');
	trackCurrentPage($location);
});

gamedevatApp.controller('HomeCtrl', function ($scope, $location) {
	$scope.data = database;
	$scope.randomCompanyAmount = 4;
	$scope.randomizeCompanies = function () {
		$scope.companiesHighlight = [];
		var i = 0;
		do {
			var item = $scope.data[Math.floor(Math.random() * $scope.data.length)];
			if ($scope.companiesHighlight.indexOf(item) < 0)
				$scope.companiesHighlight.push(item);
		} while (i < 100 && $scope.companiesHighlight.length < $scope.randomCompanyAmount)

	};


	$scope.randomizeCompanies();
	trackCurrentPage($location);
});

gamedevatApp.controller('SearchResultsCtrl', function ($scope, $routeParams, $filter, $location) {
	$scope.companies = database;
	$scope.hasSearchInput = false;
	$scope.hasNoSearchInput = !$scope.hasSearchInput;
	$scope.filterSearchTag = $routeParams.tag;
	$scope.searchQuery = $routeParams.search;
	$scope.showList = true;
	$scope.mapInitialized = false;
	$scope.setShowList = function (value) {
		if (value == $scope.showList)
			return;
		$scope.showList = value;
		$scope.mapInitialized = false;
		$scope.initializeMap();
	};

	$scope.filterSearchTagComperator = function (item) {

		if (!$scope.filterSearchTag)
			return true;

		if (!angular.isObject(item))
			return true;
		if (!angular.isArray(item.tags))
			return true;

		return item.tags.indexOf($scope.filterSearchTag) >= 0;
	};

	$scope.initializeMap = function () {
		if (!$scope.showList && !$scope.mapInitialized) {

			$scope.mapInitialized = true;

			if ($scope.map) {
				$scope.updateMapItems();
				return;
			}

			var mapOptions = {
				zoom: 7
			};

			var mapElement = document.getElementById('map-canvas');
			$scope.map = new google.maps.Map(mapElement, mapOptions);

			var defaultPos = new google.maps.LatLng(47.696472, 13.3457347); //center Austria
			// Try HTML5 geolocation
			/*
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function (position) {
					var pos = new google.maps.LatLng(position.coords.latitude,
						position.coords.longitude);
					$scope.map.setCenter(pos);
				}, function () {
					$scope.map.setCenter(defaultPos);
				});
			} else {
				$scope.map.setCenter(defaultPos);
			}
			*/
			$scope.map.setCenter(defaultPos);
		}
		$scope.updateMapItems();
	};

	$scope.updateMapItems = function () {

		setTimeout(function () {
			var currentCenter = $scope.map.getCenter();
			google.maps.event.trigger($scope.map, 'resize');
			$scope.map.setCenter(currentCenter); //recenter
		}, 200);

		if ($scope.mapInitialized && !$scope.showList) {
			if ($scope.markers) {
				$scope.markers.forEach(function (marker) {
					marker.setMap(null);
				});
			}

			$scope.markers = [];

			var result = $filter('orderBy')($scope.companies, 'title');
			result = $filter('filter')(result, $scope.filterSearchTagComperator);
			result = $filter('filter')(result, $scope.searchQuery);

			result.forEach(function (company) {
				if (!angular.isNumber(company.latitude) || !angular.isNumber(company.longitude))
					return;

				var cPos = new google.maps.LatLng(company.latitude, company.longitude);

				var newMarker = new google.maps.Marker({
					position: cPos,
					map: $scope.map,
					title: company.title,
					draggable: false,
					animation: google.maps.Animation.DROP
				});
				$scope.markers.push(newMarker);
			});
		}
	};
	$scope.$watch("searchQuery", function (newValue, oldValue) {
		$scope.hasSearchInput = (angular.isString(newValue) && newValue.length > 0);
		$scope.hasNoSearchInput = !$scope.hasSearchInput;

		$scope.updateMapItems();
	});

	trackCurrentPage($location);
});

gamedevatApp.controller('CompanyCtrl', function ($scope, $routeParams, $location) {
	angular.forEach(database, function (item) {
		if (item.companyId == $routeParams.companyId) {
			$scope.company = item;
		}
	});

	$scope.companyId = $routeParams.companyId;
	trackCurrentPage($location);
});

gamedevatApp.controller('NewCompanyCtrl', function ($scope, $routeParams, $location) {
	$scope.company = {
		"companyId": "[new]",
		"title": "",
		"website": "",
		"image": "",
		"description": "",
		"address": null,
		"latitude": 48.208202,
		"longitude": 16.373914
	};
	angular.forEach(database, function (item) {
		if (item.companyId == $routeParams.companyId) {
			$scope.company = item;
		}
	});

	$scope.companyId = $routeParams.companyId;

	$scope.save = function (company) {
		var cIndex = database.indexOf(company);
		if (cIndex >= 0) {
			database[cIndex] = angular.copy(company);
		} else {
			var newCompany = angular.copy(company);
			newCompany.companyId = newCompany.title.replace(" ", "");

			addItem(newCompany);
			cIndex = database.indexOf(newCompany);
		}

		$scope.company = database[cIndex];

		$location.path('/');
	};

	var mapOptions = {
		zoom: 16
	};

	var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
	var cPos = new google.maps.LatLng($scope.company.latitude, $scope.company.longitude);
	var title = $scope.company.title;

	map.setCenter(cPos);
	$scope.marker = new google.maps.Marker({
		position: cPos,
		map: map,
		title: title,
		draggable: true,
		animation: google.maps.Animation.DROP
	});

	google.maps.event.addListener($scope.marker, 'dragend', function () {
		var newPos = $scope.marker.getPosition();
		$scope.company.latitude = newPos.lat();
		$scope.company.longitude = newPos.lng();
		$scope.$digest(); //force update to bindings
	});

	$scope.locateAddress = function () {
		if (!$scope.company.address)
			return;

		var geoCoder = new google.maps.Geocoder()
		var geoRequest = new Object();
		geoRequest.address = $scope.company.address;
		geoRequest.region = "at";
		geoCoder.geocode(geoRequest, function (resultArray, status) {
			if (!resultArray)
				return;

			if (!Array.isArray(resultArray))
				return;

			if (resultArray.length <= 0)
				return;

			if (status != "OK")
				return;

			$scope.company.latitude = resultArray[0].geometry.location.k;
			$scope.company.longitude = resultArray[0].geometry.location.A;
			$scope.$digest(); //force update to bindings

			var cPos = new google.maps.LatLng($scope.company.latitude, $scope.company.longitude);
			$scope.marker.setPosition(cPos);
			map.setCenter(cPos);
		});
	};
	trackCurrentPage($location);
});

gamedevatApp.controller('ImpressumCtrl', function ($scope, $location) {
	trackCurrentPage($location);
});

gamedevatApp.controller('EventsCtrl', function ($scope, $location) {
	trackCurrentPage($location);
});