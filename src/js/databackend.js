var database = [];
var storedDatabase = [];
var modifiedDatabase = [];
var removedDatabase = [];

function initializeDatabase()
{
    loadDatabaseFromJson();

	//updateSearchResults();
}

function addItem(value)
{
	database.push(value);
    modifiedDatabase.push(value);
}

function removeItem(value)
{
	var index = database.indexof(value);
	if (index >= 0)
	{
		database.splice(index, 1);
        removedDatabase.push(value);
	}
}

function updateItem(value)
{
	modifiedDatabase.push(value);
}

function loadDatabaseFromJson()
{
	var jsonUrl;
	
	if (isAdmin)
		jsonUrl = "data/companies_en.json";
	else
		jsonUrl = "data/companies_en.json";
		
	var jqHDX = $.getJSON( jsonUrl );
	jqHDX.done( function( data ) {
		if (!Array.isArray(data))
		{
			console.log("Not an array");	
			return;
		}
        storedDatabase = data;
        database = data.slice(0);
        modifiedDatabase = [];
        removedDatabase = [];
		console.log("Database loaded.");		
	});
	jqHDX.fail(function () {
		console.log("Failed to open database");
	});
	jqHDX.always(function() {

	});
}